# H = sum_j mu_j Z_j + g/L sum_j,j' X_j X_j' where mu_j in [-1,1]

import numpy
import numpy.linalg
import numpy.random
import sys

def flip_spin(u,site):
    return u ^ (1 << site)

def get_spin(u,site):
    return (u >> site) & 1

def get_parity(u,L):
    sz_tot=0
    for site in range(L):
        sz_tot+=get_spin(u,site)
    return sz_tot % 2
    
if len(sys.argv) < 4:
    print('Usage: *.py g L parity={0,1} [rand_seed]')
    sys.exit()

g=float(sys.argv[1])
L=int(sys.argv[2])
parity=int(sys.argv[3])
if len(sys.argv) > 4:
    rand_seed=int(sys.argv[4])

basis=numpy.zeros((0),dtype=int)
for u in range(2**L):
    if get_parity(u,L) == parity:
        basis=numpy.append(basis,u)

basis_sz=len(basis)
print('basis_sz=',basis_sz)

# Sanity check the functions
u=basis[numpy.random.randint(basis_sz)]

H=numpy.zeros((basis_sz,basis_sz))
mu=2*numpy.random.random((L))-1

for j in range(basis_sz):
    u=basis[j]
    E_z=0
    for site in range(L):
        E_z+=mu[site]*(-1)**get_spin(u,site)
        for sitep in range(L):
            u2=flip_spin(flip_spin(u,site),sitep)
            j2=numpy.searchsorted(basis,u2)
            assert(basis[j2]==u2)
            H[j,j2]+=g/L
    H[j,j]+=E_z

(E,V)=numpy.linalg.eigh(H)

# Calculate level statistic r averaged over middle 1/2 of spectrum
gaps=E[int(basis_sz/4+1):int(3*basis_sz/4+1)]-E[int(basis_sz/4):int(3*basis_sz/4)]
r=numpy.mean(numpy.minimum(gaps[1:],gaps[:-1])/numpy.maximum(gaps[1:],gaps[:-1]))

# Calculate KLd of nn energy eigenstates in z-basis averaged over middle 1/2 
p=numpy.abs(V[:,int(basis_sz/4):int(3*basis_sz/4)])**2
lp=numpy.log(p[:,:-1]/p[:,1:])
K=numpy.sum(p[:,:-1]*lp)/float(p.shape[1]-1)

numpy.save('r.npy',r)
numpy.save('K.npy',K)

print('r=',r)
print('K=',K)

