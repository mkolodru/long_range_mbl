# H = sum_j [mu_j Z_j - J Z_j Z_{j+1} + 2 Gamma beta X_j / Omega + Gamma^2/Omega sum_j,j' X_j X_j' where mu_j in [-W,W]

import numpy
import numpy.linalg
import numpy.random
import sys

def flip_spin(u,site):
    return u ^ (1 << site)

def get_spin(u,site):
    return (u >> site) & 1

def get_parity(u,L):
    sz_tot=0
    for site in range(L):
        sz_tot+=get_spin(u,site)
    return sz_tot % 2
    
if len(sys.argv) < 7:
    print('Usage: *.py W J beta Omega Gamma L [rand_seed]')
    sys.exit()

W=float(sys.argv[1])
J=float(sys.argv[2])
beta=float(sys.argv[3])
Omega=float(sys.argv[4])
Gamma=float(sys.argv[5])
L=int(sys.argv[6])
if len(sys.argv) > 7:
    rand_seed=int(sys.argv[7])

n_parities=1
if beta == 0: # Then parity is conserved
    n_parities=2

r=numpy.zeros((0))
K=numpy.zeros((0))

for parity in range(n_parities):
    basis=numpy.zeros((0),dtype=int)
    for u in range(2**L):
        if n_parities == 1 or get_parity(u,L) == parity:
            basis=numpy.append(basis,u)

    basis_sz=len(basis)
    print('basis_sz=',basis_sz)

    # Sanity check the functions
    u=basis[numpy.random.randint(basis_sz)]

    H=numpy.zeros((basis_sz,basis_sz))
    mu=2*numpy.random.random((L))-1

    for j in range(basis_sz):
        u=basis[j]
        E_z=0
        for site in range(L):
            s1=get_spin(u,site)
            s2=get_spin(u,(site+1)%L)
            E_z+=mu[site]*(-1)**s1 - J*(-1)**(s1+s2)
            if n_parities==1:
                u2=flip_spin(u,site)
                j2=numpy.searchsorted(basis,u2)
                assert(basis[j2]==u2)
                H[j,j2]+=2*Gamma*beta/Omega
            for sitep in range(L):
                u2=flip_spin(flip_spin(u,site),sitep)
                j2=numpy.searchsorted(basis,u2)
                assert(basis[j2]==u2)
                H[j,j2]+=(Gamma**2)/Omega
        H[j,j]+=E_z

    (E,V)=numpy.linalg.eigh(H)

    # Calculate level statistic r averaged over middle 1/2 of spectrum
    gaps=E[int(basis_sz/4+1):int(3*basis_sz/4+1)]-E[int(basis_sz/4):int(3*basis_sz/4)]
    r=numpy.append(r,numpy.mean(numpy.minimum(gaps[1:],gaps[:-1])/numpy.maximum(gaps[1:],gaps[:-1])))

    # Calculate KLd of nn energy eigenstates in z-basis averaged over middle 1/2 
    p=numpy.abs(V[:,int(basis_sz/4):int(3*basis_sz/4)])**2
    lp=numpy.log(p[:,:-1]/p[:,1:])
    K=numpy.append(K,numpy.sum(p[:,:-1]*lp)/float(p.shape[1]-1))

numpy.save('r.npy',numpy.mean(r))
numpy.save('K.npy',numpy.mean(K))

print('r=',r)
print('K=',K)

